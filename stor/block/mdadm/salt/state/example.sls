/dev/md0:
  raid.present:
    - level: 0
    - devices:
      - /dev/sdb
      - /dev/sdc
    - chunk: 256
    - run: True
