# All devices will belong to a Pool named: 'fastest' 'fast' 'slow' 'hybrid' 'external' or 'nbd'

```
fastest: nvme
fast: SSDs
standard: HDs
hybrid: combine both :-)
external: firewire & usb devices!
nbd: NBD, Sheepdog, Ceph RBD, and iSCSI
```

# Get info:
`ls -l /dev/disk/by-id/``
